# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=


# Set a fancy prompt following https://kate-editor.org/2018/01/08/fancy-terminal-prompt/
# with some suggestions from the comments therein
source /usr/share/git-core/contrib/completion/git-prompt.sh
PS1="\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]"
PS1="$PS1 \`_E=\$?; if [ \$_E = 0 ]; then echo -e '\[\033[01;32m\]:-)';"
PS1="$PS1 else echo -e '\[\033[01;31m\]:-(' \$_E; fi\`\[\033[00m\]"
PS1="$PS1 \$(__git_ps1 \"(%s)\") \$ "

# User specific aliases and functions
export EDITOR=kate

export STAGE_HOST=castorpublic
export EOS_MGM_URL=root://eosproject.cern.ch
export EOS_HOME=/eos/project/w/wccoasvc # or anything else you may want;
