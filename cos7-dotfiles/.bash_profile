# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH
# VM-70 enable "scl" newer versions of svn, git, pidgin-vv
source scl_source enable sclo-git212 

#VM-83: make sure original man-page paths are included after SCL
export MANPATH=$MANPATH:/usr/share/man

#VM-100, VM-104: set KRB5CCNAME required by dist-composer
export KRB5CCNAME=/tmp/krb5cc_${UID}
										
# Make kobe (component builder) available in the PATH
export PATH=$PATH:$HOME/workspace/component_builder

# make sure dist_composer will be able to start UI processes
xhost +SI:localuser:root 2>&1>/dev/null
