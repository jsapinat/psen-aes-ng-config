#!/bin/sh

# Project must be mounted on /opt/unicryo
# Must register existing project

echo "Copying Next Generation AES button for PSEN Ui"
cp -rf ./PVSS_psenAlarmScreenNg/panels/objects/UN_GRAPHICALFRAME/unNgAlarmListButton.pnl /opt/unicryo/PVSS_projects/psen/psen/panels/objects/UN_GRAPHICALFRAME/unNgAlarmListButton.pnl

echo "Fixing paths for PSEN Next Generation AES"

echo "Creating vision directory"
mkdir -p /opt/unicryo/PVSS_projects/psen/psen/panels/vision/psenAlarmScreenNg

echo "Creating scripts library directory"
mkdir -p /opt/unicryo/PVSS_projects/psen/psen/scripts/libs/psenAlarmScreenNg

echo "Creating dplist directory"
mkdir -p /opt/unicryo/PVSS_projects/psen/psen/dplist/psenAlarmScreenNg

echo "Installing PSEN Next Generation AES panels"
cp ./PVSS_psenAlarmScreenNg/panels/vision/psenAlarmScreenNg/*.xml /opt/unicryo/PVSS_projects/psen/psen/panels/vision/psenAlarmScreenNg/

echo "Installing PSEN Next Generation AES scripts"
cp ./PVSS_psenAlarmScreenNg/scripts/libs/psenAlarmScreenNg/*.ctl /opt/unicryo/PVSS_projects/psen/psen/scripts/libs/psenAlarmScreenNg/

echo "Installing PSEN Next Generation AES dplits"
cp ./PVSS_psenAlarmScreenNg/dplist/psenAlarmScreenNg/*.dpl /opt/unicryo/PVSS_projects/psen/psen/dplist/psenAlarmScreenNg/

echo "Updating PSEN faceplates for Next Generation AES"
cp ./PVSS_psenAlarmScreenNg/panels/objects/PSEN_EL_Faceplate/* /opt/unicryo/PVSS_projects/psen/installed_components/panels/objects/PSEN_EL_Faceplate/

echo "Updating PSEN binaries for Next Generation AES"
cp ./build-Source-WinCCOA_Dev-Debug/libs/*.so /opt/unicryo/PVSS_projects/psen/installed_components/bin/plugins/

# ASCII Import from /PVSS_psenAlarmScreenNg/dplist/psenAlarmScreenNg/
