#!/bin/bash

QT_ON_EOS_DIR=/eos/project/j/jcopfw/dependencies/Qt/qt-wccoa319/5.15.11-15
QT_ARCHIVE=qt-wccoa319-5.15.11-15-minimal-al9.tgz
NEXUS_URL=https://repository.cern.ch/nexus/service/local/repositories
NEXUS_REPO=cern-winccoa-3rdparty
NEXUS_PATH=cern/winccoa/Qt/qt-wccoa319
QT_NEXUS_DIR=${NEXUS_URL}/${NEXUS_REPO}/content/${NEXUS_PATH}/5.15.11-15

cd /opt/Qt

if [ -d ${QT_ON_EOS_DIR} ] && [ -r ${QT_ON_EOS_DIR} ]; then
	echo "OK. Qt acceessible on EOS: ${QT_ON_EOS_DIR}";

	if [ -r ${QT_ON_EOS_DIR}/${QT_ARCHIVE} ];then
		echo "Extracting Qt distribution from  ${QT_ON_EOS_DIR}/${QT_ARCHIVE}"
		tar xvzf  ${QT_ON_EOS_DIR}/${QT_ARCHIVE}
	else
		echo "ERROR: Qt distribution not accessible:  ${QT_ON_EOS_DIR}/${QT_ARCHIVE}";
		exit 1;
	fi
else
	echo "No access through EOS. Trying to download from Nexus"
	wget -N $QT_NEXUS_DIR/$QT_ARCHIVE
	if [ $? -ne 0 ]; then
		echo "ERROR. Could not download $QT_NEXUS_DIR/$QT_ARCHIVE";
		exit 1;
	fi
	tar xvzf ./${QT_ARCHIVE}
	if [ $? -ne 0 ]; then
		echo "There was an error extracting the file ./${QT_ARCHIVE}"
		exit 1
	fi
	rm ./${QT_ARCHIVE}
fi
