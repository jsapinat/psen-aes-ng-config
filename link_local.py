# (c) Copyright CERN 2005. All rights not expressly granted are reserved.
# icecontrols.support@cern.ch
#
# SPDX-License-Identifier: Apache-2.0


"""
Cross platform script to copy necessary binaries such as .so and .ewo
and to symlink other necessary files such as xml, pnl etc to the appropriate WinCC OA project folders.

Meant to be run within the WinCC OA enabled Guest OS.
"""

import shutil
import os
import platform
import hashlib

from pathlib import Path
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("file_path", type=Path, help="path to fwcomponents folder")
args = parser.parse_args()

SRC_PATH = Path.cwd()
COMP_PATH = args.file_path
BIN_EXT = ".dll" if platform.system() == "Windows" else ".so"


def hash_file(filename):
    hasher = hashlib.md5()
    with open(filename, "rb") as f:
        while True:
            chunk = f.read(4096)
            if not chunk:
                break
            hasher.update(chunk)
    return hasher.hexdigest()


def symlink_file(src, dst, *args):
    if (
        "bin" in src
        and (src.endswith(BIN_EXT) or src.endswith(".ewo"))
        and os.path.isfile(src)
    ):
        dst = dst.rsplit("/", maxsplit=2)
        dst.pop(1)
        dst = "/".join(dst)

        src_hash = hash_file(src)
        print(f"Copying {src}  ->  {dst}")
        shutil.copy2(src, dst)
        dst_hash = hash_file(dst)
        print(src_hash, dst_hash)
        if src_hash != dst_hash:
            print("File NOT COPIED!!")
        return

    if os.path.exists(dst):
        os.remove(dst)
    print(f"Symlinking {src}  ->  {dst}")
    os.symlink(src, dst)


for pvss_dir in SRC_PATH.glob("PVSS_*"):
    shutil.copytree(
        pvss_dir,
        COMP_PATH,
        copy_function=symlink_file,
        ignore=shutil.ignore_patterns(
            "*.doxy_ext", "*.sty"
        ),
        dirs_exist_ok=True,
    )

print("Done")
