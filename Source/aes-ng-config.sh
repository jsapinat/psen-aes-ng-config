#!/bin/bash

# (c) Copyright CERN 2005. All rights not expressly granted are reserved.
# icecontrols.support@cern.ch
#
# SPDX-License-Identifier: Apache-2.0




AES_NG_BUILD_TYPE="Release"

AES_NG_COMPONENTS+=" models"
AES_NG_COMPONENTS+=" EWO/common"
AES_NG_COMPONENTS+=" EWO/AlarmsEventsEWO"
AES_NG_COMPONENTS+=" EWO/AsFilterStatistics"
AES_NG_COMPONENTS+=" plugins/alarmSource/JCOP"
AES_NG_COMPONENTS+=" plugins/alarmSource/UNICOS"
AES_NG_COMPONENTS+=" plugins/alarmSource/PSEN"
AES_NG_COMPONENTS+=" plugins/filterWidget/JCOP"
AES_NG_COMPONENTS+=" plugins/filterWidget/UNICOS"
AES_NG_COMPONENTS+=" plugins/filterWidget/PSEN"
