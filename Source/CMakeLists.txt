cmake_minimum_required(VERSION 3.1)

# Set environment variables 
set(API_ROOT "$ENV{API_ROOT}" CACHE FILEPATH "Directory of the WinCC_OA API installation")
set(QTDIR "$ENV{QTDIR}" CACHE FILEPATH "Directory of the Qt installation")

# Include global configuration
include(${API_ROOT}/CMakeDefines.txt)

# Set C++ standard globally
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Set project identifier
project(AlarmModelsNg)

# Add subdirectories for each component of the project
add_subdirectory(models)
add_subdirectory(EWO)
add_subdirectory(plugins)
