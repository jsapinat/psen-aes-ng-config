#!/bin/bash

# (c) Copyright CERN 2005. All rights not expressly granted are reserved.
# icecontrols.support@cern.ch
#
# SPDX-License-Identifier: Apache-2.0




set -e

if [[ -z "${API_ROOT}" ]]; then
    echo "API_ROOT is not set. Forgot to source the env.sh script?"
    exit 1;
fi

. aes-ng-config.sh

for component in ${AES_NG_COMPONENTS[@]} ; do
    echo "BUILDING: $component"
    if [ "$1" == "clean" ]; then
        rm -rf $component/build
    fi
    mkdir -p $component/build
    cmake -S $component -B $component/build
    cmake --build $component/build --config ${AES_NG_BUILD_TYPE}
done
