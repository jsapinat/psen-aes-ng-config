#include "GroupingAlarmsProxyModel.hxx"

#include "AlarmsModel.hxx"
#include "AlarmsProxyModel.hxx"
#include "AlarmsProxyModelGroup.hxx"

GroupingAlarmsProxyModel::GroupingAlarmsProxyModel(const GroupingConfiguration &groupConfig, AlarmsProxyModel *source, QObject *parent) :
    QAbstractItemModel(parent),
    MsgLoggingProducer(&groupConfig),
    m_proxyModel(source),
    m_config(groupConfig),
    m_rootGroup(new AlarmsProxyModelGroup(&groupConfig)),
    m_firstRowToRemove(-1),
    m_lastRowToRemove(-1)
{
    // Manipulations with columns are not expected, just ignore related signals

    // Other signals of source model are processed by this model
    connect(m_proxyModel, SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &, const QVector<int> &)),
            this, SLOT(srcDataChanged(const QModelIndex &, const QModelIndex &, const QVector<int> &)));
    connect(m_proxyModel, SIGNAL(modelAboutToBeReset()),
            this, SIGNAL(modelAboutToBeReset()));
    connect(m_proxyModel, SIGNAL(modelReset()),
            this, SLOT(srcModelReset()));
    /* TODO: before rows are really inserted in source model, I don't know where shall they appear in this model
    connect(m_proxyModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex &, int, int)),
            this, SLOT(srcRowsAboutToBeInserted(const QModelIndex &, int, int)));
    */
    connect(m_proxyModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
            this, SLOT(srcRowsInserted(const QModelIndex &, int, int)));
    /* TODO: do not expect the source rows to be moved...
    connect(m_proxyModel, SIGNAL(rowsAboutToBeMoved(const QModelIndex &, int, int, const QModelIndex &, int)),
            this, SLOT(srcRowsAboutToBeMoved(const QModelIndex &, int, int, const QModelIndex &, int)));
    connect(m_proxyModel, SIGNAL(rowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)),
            this, SLOT(srcRowsMoved(const QModelIndex &, int, int, const QModelIndex &, int)));
    */
    connect(m_proxyModel, SIGNAL(rowsAboutToBeRemoved(const QModelIndex &, int, int)),
            this, SLOT(srcRowsAboutToBeRemoved(const QModelIndex &, int, int)));
    connect(m_proxyModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
            this, SLOT(srcRowsRemoved(const QModelIndex &, int, int)));
}

GroupingAlarmsProxyModel::~GroupingAlarmsProxyModel()
{
    delete m_proxyModel;    // TODO: may be child model may have sense?
}

void GroupingAlarmsProxyModel::setGroupingConfig(const GroupingConfiguration &groupConfig)
{
    m_config = groupConfig;
    rebuildModel();
}

void GroupingAlarmsProxyModel::rebuildModel()
{
    beginResetModel();
    m_rootGroup.reset(new AlarmsProxyModelGroup(AlarmsGroupRule(this), QVariant()));
    m_alarmGroups.clear();
    m_rowAlarms.clear();
    buildGroups();
    endResetModel();
}

void GroupingAlarmsProxyModel::buildGroups()
{
    for(int row = 0 ; row < m_proxyModel->rowCount(); row++)
    {
        QSharedPointer<AlarmInstance> alarm = m_proxyModel->getAlarmAtRow(m_proxyModel->index(row, 0));
        AlarmsProxyModelGroup *group = m_rootGroup->addAlarmToGroup(alarm, m_config);
        m_alarmGroups.insert(alarm, group);
        m_rowAlarms.append(alarm);
    }
}

/*=============================== QAbstractItemModel interface ==================================================*/

QModelIndex GroupingAlarmsProxyModel::index(int row, int column, const QModelIndex &parent) const
{
#ifdef DEEP_GROUPING_MODEL_DEBUG
    QModelIndex result = indexPrivate(row, column, parent);
    NG_MSG_WARNING(this) "index for (" << row << ", " << column << " of parent "
                               << indexToString(parent) << " is: " << indexToString(result);
    return result;
}

QModelIndex GroupingAlarmsProxyModel::indexPrivate(int row, int column, const QModelIndex &parent) const
{
#endif  // DEEP_GROUPING_MODEL_DEBUG

    // First find a parent group. Every valid index contains internalPointer == pointer to group,
    // invalid parent pointer implicitely assumes the group m_rootGroup
    AlarmsProxyModelGroup *parentGroup = m_rootGroup.data();
    if(parent.isValid())
    {
        parentGroup = static_cast<AlarmsProxyModelGroup *>(parent.internalPointer());
        parentGroup = parentGroup->getChildGroup(parent.row());
    }
    if(!parentGroup)    // This shall not happen
    {
        NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::index(): NULL parent group";
        return QModelIndex();
    }
    if((0 <= row) && (row <= parentGroup->getChildrenCount()))
    {
        return createIndex(row, column, parentGroup);
    }
    else
    {
        NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::index(): invalid row " << row
                      << " for group " << parentGroup->toString();
    }
    return QModelIndex();
}

QModelIndex GroupingAlarmsProxyModel::indexOfGroup(AlarmsProxyModelGroup *group) const
{
    if(group)
    {
        AlarmsProxyModelGroup *parent = group->getParent();
        if(parent)
        {
            return createIndex(group->getRowInParent(), 0, parent);
        }
    }
    return QModelIndex();
}

QModelIndex GroupingAlarmsProxyModel::parent(const QModelIndex &child) const
{
#ifdef DEEP_GROUPING_MODEL_DEBUG
    QModelIndex result = parentPrivate(child);
    NG_MSG_WARNING(this) << "parent of " << indexToString(child) << " is: " << indexToString(result);
    return result;
}

QModelIndex GroupingAlarmsProxyModel::parentPrivate(const QModelIndex &child) const
{
#endif  // DEEP_GROUPING_MODEL_DEBUG
    if(child.isValid())
    {
        AlarmsProxyModelGroup *groupOfIndex = static_cast<AlarmsProxyModelGroup *>(child.internalPointer());
        return indexOfGroup(groupOfIndex);
    }
    return QModelIndex();
}

int GroupingAlarmsProxyModel::rowCount(const QModelIndex &parent) const
{
#ifdef DEEP_GROUPING_MODEL_DEBUG
    int result = rowCountPrivate(parent);
    NG_MSG_WARNING(this) << "rowCount of " << indexToString(parent) << " is: " << result;
    return result;
}

int GroupingAlarmsProxyModel::rowCountPrivate(const QModelIndex &parent) const
{
#endif  // DEEP_GROUPING_MODEL_DEBUG
    if(parent.column() > 0)
    {
        return 0;   // The agreement is: only column 0 may have children
    }
    AlarmsProxyModelGroup *group = m_rootGroup.data();
    if(parent.isValid())
    {
        group = static_cast<AlarmsProxyModelGroup *>(parent.internalPointer());
        group = group->getChildGroup(parent.row());
    }
    return group ? group->getChildrenCount() : 0;
}

int GroupingAlarmsProxyModel::columnCount(const QModelIndex &parent) const
{
#ifdef DEEP_GROUPING_MODEL_DEBUG
    int result = columnCountPrivate(parent);
    NG_MSG_WARNING(this) << "columnCount of " << indexToString(parent) << " is: " << result;
    return result;
}

int GroupingAlarmsProxyModel::columnCountPrivate(const QModelIndex &parent) const
{
#endif  // DEEP_GROUPING_MODEL_DEBUG
    return m_proxyModel->columnCount(parent) + 1;  // Column #0 is added to display groups (tree)
}

QVariant GroupingAlarmsProxyModel::data(const QModelIndex &index, int role) const
{
#ifdef DEEP_GROUPING_MODEL_DEBUG
    QVariant result = dataPrivate(index, role);
    NG_MSG_WARNING(this) << "data for role " << role << " of " << indexToString(index)
                 << " is: " << result.toString();
    return result;
}

QVariant GroupingAlarmsProxyModel::dataPrivate(const QModelIndex &index, int role) const
{
#endif  // DEEP_GROUPING_MODEL_DEBUG
    if(!index.isValid())
    {
        return QVariant();
    }
    AlarmsProxyModelGroup *group = static_cast<AlarmsProxyModelGroup*>(index.internalPointer());
    if(group->childrenAreGroups())
    {
        if(index.column() == 0) // Grouping column
        {
            return group->data(index.row(), role);
        }
        else
        {
            return QVariant();
        }
    }
    else if(index.column() > 0)
    {
        QSharedPointer<AlarmInstance> alarm = group->getAlarmAteRow(index.row());
        if(alarm)
        {
            return m_proxyModel->data(alarm, index.column() - 1, role); // column 0 is group, not known by underlying proxy model
        }
    }
    return QVariant();
}

QString GroupingAlarmsProxyModel::indexToString(const QModelIndex &index) const
{
    if(!index.isValid())
    {
        return QString("invalid: row %1 column %2 model %3").arg(index.row()).arg(index.column())
                .arg(index.model() == this ? "this" : (index.model() == nullptr ? "null" : "OTHER"));
    }
    QString result = QString("{row %1, column %2, group ").arg(index.row()).arg(index.column());
    AlarmsProxyModelGroup *group = static_cast<AlarmsProxyModelGroup*>(index.internalPointer());
    if(group)
    {
        result += group->toString();
    }
    else
    {
        result += "NULL";
    }
    result += "}";
    return result;

}

const QSharedPointer<AlarmInstance> GroupingAlarmsProxyModel::getAlarmAtRow(const QModelIndex &index) const
{
    if(!index.isValid())
    {
        return QSharedPointer<AlarmInstance>();
    }
    AlarmsProxyModelGroup *group = static_cast<AlarmsProxyModelGroup*>(index.internalPointer());
    return group->getAlarmAteRow(index.row());
}

AlarmsModelColumn GroupingAlarmsProxyModel::getColumnDefinition() const
{
    AlarmsModelColumn result(this);
    result.setTitle(m_config.getGroupHeader());
    result.setWidth(m_config.getColumnWidth());
    result.setVisibility(Constants::ColumnVisibility::VisibleAlways);    // Grouping column is always visible
    return result;
}

void GroupingAlarmsProxyModel::setColumnWidth(int width)
{
    m_config.setColumnWidth(width);
}

int GroupingAlarmsProxyModel::compareRows(const QModelIndex &indexLeft,
                                          const QModelIndex &indexRight, bool reverse) const
{
    if(!(indexLeft.isValid() && indexRight.isValid()))
    {
        return 0;   // we need valid inidices for decision
    }
    AlarmsProxyModelGroup *groupLeft = static_cast<AlarmsProxyModelGroup*>(indexLeft.internalPointer());
    if(!groupLeft->childrenAreGroups())
    {
        return 0;   // indexLeft does not correspond to grouping row
    }
    AlarmsProxyModelGroup *groupRight = static_cast<AlarmsProxyModelGroup*>(indexRight.internalPointer());
    if(groupLeft != groupRight)
    {
        return 0;   // indices for items in different groups
    }
    AlarmsProxyModelGroup *itemGroupLeft = groupLeft->getChildGroup(indexLeft.row()),
                          *itemGroupRight = groupRight->getChildGroup(indexRight.row());
    if(itemGroupLeft->getMainValue() < itemGroupRight->getMainValue())
    {
        return (-1 * (itemGroupLeft->isAscendingSorting() ? 1 : -1) * (reverse ? -1 : 1));
    }
    else if(itemGroupLeft->getMainValue() > itemGroupRight->getMainValue())
    {
        return (1 * (itemGroupLeft->isAscendingSorting() ? 1 : -1) * (reverse ? -1 : 1));
    }
    return 0;
}

bool GroupingAlarmsProxyModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(index)
    Q_UNUSED(value)
    Q_UNUSED(role)
    return false;   // The model is not ediatble... though... the value of group column? TODO
}

QVariant GroupingAlarmsProxyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
#ifdef DEEP_GROUPING_MODEL_DEBUG_2
    QVariant result = headerDataPrivate(section, orientation, role);
    NG_MSG_WARNING(this) << "headerData(" << section << ", " << orientation << ", " << role
                 << ") is: " << result.toString();
    return result;
}

QVariant GroupingAlarmsProxyModel::headerDataPrivate(int section, Qt::Orientation orientation, int role) const
{
#endif  // DEEP_GROUPING_MODEL_DEBUG_2
    if((role == Qt::DisplayRole) && (orientation == Qt::Horizontal))
    {
        if(section == 0)
        {
            return m_config.getGroupHeader();
        }
        return m_proxyModel->headerData(section - 1, orientation, role);
    }
    return QAbstractItemModel::headerData(section, orientation, role);
}

bool GroupingAlarmsProxyModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(value)
    Q_UNUSED(role)
    return false;   // The model is not ediatble... though... the header of group column? TODO
}

Qt::ItemFlags GroupingAlarmsProxyModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);    // TODO: may be different flags for column 0?
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;

}
void GroupingAlarmsProxyModel::sort(int column, Qt::SortOrder order)
{
    Q_UNUSED(column)    // TODO: this model is not sortable... but may be sortable on column 0 ?
    Q_UNUSED(order)
}

QModelIndexList GroupingAlarmsProxyModel::match(const QModelIndex &start, int role, const QVariant &value, int hits, Qt::MatchFlags flags) const
{
    Q_UNUSED(start)
    Q_UNUSED(role)
    Q_UNUSED(value)
    Q_UNUSED(hits)
    Q_UNUSED(flags)
    return QModelIndexList();   // TODO ?
}

bool GroupingAlarmsProxyModel::canFetchMore(const QModelIndex &parent) const
{
#ifdef DEEP_GROUPING_MODEL_DEBUG
    bool result = canFetchMorePrivate(parent);
    NG_MSG_WARNING(this) << "canFetchMore() is " << result << " for parent " << indexToString(parent);
    return result;
}

bool GroupingAlarmsProxyModel::canFetchMorePrivate(const QModelIndex &parent) const
{
#endif  // DEEP_GROUPING_MODEL_DEBUG
    bool result = QAbstractItemModel::canFetchMore(parent);
    return result;
}

bool GroupingAlarmsProxyModel::submit()
{
    return true;
}

void GroupingAlarmsProxyModel::revert()
{
}

/*================================== Slots for connection to 'standard' signals of source model ====================*/

void GroupingAlarmsProxyModel::srcDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    NG_MSG_DEBUG(this) << "start processing:  from " << m_proxyModel->modelIndexToString(topLeft)
                       << " to " << m_proxyModel->modelIndexToString(bottomRight);
    for(int row = topLeft.row() ; row <= bottomRight.row() ; row++)
    {
        // Because of the way alarms are modified in alarms model, we will most
        // probably get another AlarmInstance, but with the same alarm ID as old alarm
        QSharedPointer<AlarmInstance> newAlarm = m_proxyModel->getAlarmAtRow(m_proxyModel->index(row, 0));
        QSharedPointer<AlarmInstance> alarm = m_rowAlarms.at(row);
        if(!(alarm->getIdent() == newAlarm->getIdent()))
        {
            NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::srcDataChanged(): alarm ID mistamtch at row "
                          << row << ": old " << alarm->getIdent().toString()
                          << ", new " << newAlarm->getIdent().toString();
        }
        AlarmsProxyModelGroup *group = m_alarmGroups.value(alarm);
        if(!group)
        {
            NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::srcDataChanged(): group for alarm in row "
                                  << row << " is not found in srcDataChanged()) " << alarm->getIdent().toString();
            continue;
        }
        int rowInGroup = group->getRowInGroup(alarm);
        if(newAlarm != alarm)
        {
            NG_MSG_DEBUG(this) << "replace alarm at row " << row << ", new is " << newAlarm->toString();
            m_rowAlarms[row] = newAlarm;
            m_alarmGroups.remove(alarm);
            m_alarmGroups.insert(newAlarm, group);
            rowInGroup = group->replaceAlarm(alarm, newAlarm, rowInGroup);
            alarm = newAlarm;
        }
        if(rowInGroup < 0)
        {
            NG_MSG_CRITICAL(this) << "internal: alarm " << alarm->toString() << " for source row " << row << " is not found in group "
                          << group->toString();
            continue;
        }

        // two variants are possible: either just data change, or move to another group as a result of data change
        if(group->alarmMatches(alarm))  // Alarm remains in the same group
        {
            NG_MSG_DEBUG(this) << "alarm in the same group at row " << rowInGroup << ", alarm: " << alarm->toString();
            // TODO: may be something in group could also change because of data change?
            emit dataChanged(createIndex(rowInGroup, 0, group), createIndex(rowInGroup, m_proxyModel->columnCount() - 1, group), roles);
        }
        else    // Alarm shall be moved to another group
        {
            NG_MSG_DEBUG(this) << "moving alarm at row " << rowInGroup << " to another group, alarm: " << alarm->toString();
            // TODO: beginMoveRows()/endMoveRows() looks more appropriate, but what if destination group does not exists at this moment?
            GroupModelUpdateBatch batch(this);
            batch.removeAlarm(alarm, group, rowInGroup);
            batch.moveUp(); // This can be the very last row (alarm) in this group, so the whole group to be removed
            beginRemoveRows(indexOfGroup(batch.m_group), batch.m_firstGroupRow, batch.m_lastGroupRow);
            batch.m_group->removeChildrenRows(batch.m_firstGroupRow, batch.m_lastGroupRow);
            endRemoveRows();
            notifyGroupChange(batch.m_group);

            group = placeAlarmToGroup(alarm);
        }
    }
    NG_MSG_DEBUG(this) << "finish processing";
}

void GroupingAlarmsProxyModel::srcModelReset()
{
    rebuildModel();
}

/* TODO: before rows are really inserted in source model, I don't know where shall they appear in this model
void GroupingAlarmsProxyModel::srcRowsAboutToBeInserted(const QModelIndex &parent, int start, int end)
{
}
*/

void GroupingAlarmsProxyModel::srcRowsInserted(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent)
    m_updateBatches.clear();    // Doesn't make sense, must be clean before this call
    if(prepareInsertRows(first, last))
    {
        applyInsertRows();
    }
    // Clear - for next usage
    m_updateBatches.clear();
}

bool GroupingAlarmsProxyModel::prepareInsertRows(int first, int last)
{
    for(int row = first ; row <= last ; row++)
    {
        QSharedPointer<AlarmInstance> alarm = m_proxyModel->getAlarmAtRow(m_proxyModel->index(row, 0));
        m_rowAlarms.insert(row, alarm);
        int level = 0;
        AlarmsProxyModelGroup *group = m_rootGroup->findGroupForAlarmAddition(alarm, m_config, level);
        QVariant nextMainGroupValue;
        if(level < m_config.getRules().size())
        {
            const AlarmsGroupRule &rule = *(m_config.getRules()[level]);
            nextMainGroupValue = alarm->getProperty(rule.getAlarmPropId(), rule.getGroupingRole(), false);    // TODO: may be grouping by 'old' value is also allowed?
        }
        if(!m_updateBatches.contains(group))
        {
            m_updateBatches.insert(group, GroupModelUpdateBatch(this));
        }
        auto const iter = m_updateBatches.find(group);  // must find
        if(!iter.value().addAlarm(alarm, group, nextMainGroupValue, level))
        {
            NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::prepareInsertRows() failed";
            return false; // This shall not happen, something went wrong
        }
    }
    return true;
}

void GroupingAlarmsProxyModel::applyInsertRows()
{
    for(auto &batch: m_updateBatches)
    {
        int startRow, endRow;
        if(!batch.getRowsToInsert(startRow, endRow))
        {
            NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::applyInsertRows(): getRowsToInsert() failed";
            break;
        }
        QModelIndex index = indexOfGroup(batch.m_group);
#ifdef DEEP_GROUPING_MODEL_DEBUG
        NG_MSG_WARNING(this) << "beginInsertRows(): index " << indexToString(index) << ", rows range "
                     << startRow << "..." << endRow;
#endif  // DEEP_GROUPING_MODEL_DEBUG
        beginInsertRows(index, startRow, endRow);
        for(auto const &set: batch.m_sourceSets)
        {
            for(int itemIdx = 0 ; itemIdx < set.m_alarms.size() ; itemIdx++)
            {
                QSharedPointer<AlarmInstance> alarm = set.m_alarms.at(itemIdx);
                AlarmsProxyModelGroup *groupOfAlarm = batch.m_group->addAlarmToGroup(alarm, m_config,
                            set.m_groupingRulesLevel);
                m_alarmGroups.insert(alarm, groupOfAlarm);
            }
        }
#ifdef DEEP_GROUPING_MODEL_DEBUG
        NG_MSG_WARNING(this) << "endInsertRows, group" << batch.m_group->toString();
#endif  // DEEP_GROUPING_MODEL_DEBUG
        endInsertRows();
        notifyGroupChange(batch.m_group);
    }
}

/* TODO: do not expect the source rows to be moved...
void srcRowsAboutToBeMoved(const QModelIndex &sourceParent, int sourceStart, int sourceEnd, const QModelIndex &destinationParent, int destinationRow);
void srcRowsMoved(const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);
*/

void GroupingAlarmsProxyModel::srcRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent)    // Assuming that the source model is flat table
    if(m_firstRowToRemove >= 0)
    {
        NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::srcRowsAboutToBeRemoved(): unexpected invocation with range "
                      << first << "..." << last << ", while model is prepared for range "
                      << m_firstRowToRemove << "..." << m_lastRowToRemove;
        // unexpected call, but let's continue anyway
    }

    m_updateBatches.clear();
    m_firstRowToRemove = first;
    m_lastRowToRemove = last;
    for(int row = last ; row >= first ; row--)
    {
        //QSharedPointer<AlarmInstance> alarm = m_proxyModel->getAlarmAtRow(m_proxyModel->index(row, 0, parent));
        QSharedPointer<AlarmInstance> alarm = m_rowAlarms.value(row);
        
        // Review: On Alarm Screens that display only a row for CAME OR WENT (i.e. not CAME AND WENT) the previous check does not make sense.
        //         What will happen is that every 2 rows (i.e. every other iteration) an error will be displayed because no corresponding CAME or WENT for an alarm is present
        //         We had this problem happen to us on the PSEN alarm screen as rows only display the latest state, CAME or WENT, not CAME and WENT
        //         This solution is just a temporary band aid
        if(!alarm)
        {
            continue;
        }
        AlarmsProxyModelGroup *group = m_alarmGroups.value(alarm);
        if(!group)
        {
            NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::srcRowsAboutToBeRemoved(): no group found for row " << row
                          << ", alarm " << alarm->toString();
            continue;
        }
        if(!addRemovalToExistingBatch(alarm, group))
        {
            GroupModelUpdateBatch batch(this);
            batch.removeAlarm(alarm, group);
            m_updateBatches.insertMulti(group, batch);
        }
    }
    // Compress batches to the least possible number
    bool compressed = false;
    do
    {
        compressed = compressRemovalInGroups();
        compressed |= compressRemovalOnParents();
    } while(compressed);
}

bool GroupingAlarmsProxyModel::addRemovalToExistingBatch(const QSharedPointer<AlarmInstance> &alarm, AlarmsProxyModelGroup *group)
{
    int numberOfBatchesForGroup = m_updateBatches.count(group);
    if(numberOfBatchesForGroup == 1)
    {
        if(!m_updateBatches.contains(group))
        {
            m_updateBatches.insert(group, GroupModelUpdateBatch(this));
        }
        auto iter = m_updateBatches.find(group);   // must find
        return iter.value().removeAlarm(alarm, group);
    }
    else if(numberOfBatchesForGroup > 1)
    {
        QList<GroupModelUpdateBatch> batches = m_updateBatches.values(group);   // Several batches for the same group
        bool alarmProcessed = false;
        for(int batchIdx = 0 ; batchIdx < batches.size() ; batchIdx++)
        {
            alarmProcessed = batches[batchIdx].removeAlarm(alarm, group);
            if(alarmProcessed)
            {
                m_updateBatches.remove(group);
                for(auto const &batch: batches)
                {
                    m_updateBatches.insertMulti(group, batch);
                }
                break;
            }
        }
        return alarmProcessed;
    }
    return false;
}

bool GroupingAlarmsProxyModel::compressRemovalInGroups()
{
    // Now make processing
    bool result = false;
    QList<AlarmsProxyModelGroup *> groupKeys = m_updateBatches.keys();
    for(auto group: groupKeys)
    {
        if(m_updateBatches.count(group) < 2)
        {
            continue;   // Nothing to compress for this group
        }
        QList<GroupModelUpdateBatch> batches = m_updateBatches.values(group);
        if(!compressBatchesForGroup(batches))
        {
            continue;
        }
        result = true;
        m_updateBatches.remove(group);
        for(auto const &batch: batches)
        {
            m_updateBatches.insertMulti(group, batch);
        }
    }
    return result;
}

bool GroupingAlarmsProxyModel::compressBatchesForGroup(QList<GroupModelUpdateBatch> &batchesForGroup)
{
    bool result = false,
            iterationResult = false;
    do
    {
        iterationResult = false;
        for(int outer = batchesForGroup.size() - 1 ; outer > 0 ; outer--)
        {
            const GroupModelUpdateBatch &outerBatch = batchesForGroup.at(outer);
            for(int inner = outer - 1 ; inner >= 0 ; inner--)
            {
                iterationResult = batchesForGroup[inner].joinOther(outerBatch);
                if(iterationResult)
                {
                    result = true;
                    batchesForGroup.removeAt(outer);
                    break;
                }
            }
        }
    } while(iterationResult);
    return result;
}

bool GroupingAlarmsProxyModel::compressRemovalOnParents()
{
    bool result = false;
    for(auto &batch: m_updateBatches)
    {
        if(batch.moveUp())
        {
            result = true;
        }
    }
    if(result)
    {
        // Rebuild the map - now batches can be stored under wrong key because group could change for batches
        QList<GroupModelUpdateBatch> allbatches = m_updateBatches.values();
        m_updateBatches.clear();
        for(auto const &batch: allbatches)
        {
            m_updateBatches.insertMulti(batch.m_group, batch);
        }
    }
    return result;
}

void GroupingAlarmsProxyModel::srcRowsRemoved(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(parent)    // Assuming that the source model is flat table

    if((first != m_firstRowToRemove) || (last != m_lastRowToRemove))
    {
        if(m_firstRowToRemove >= 0)
        {
            NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::srcRowsRemoved(): rows range removed "
                          << first << "..." << last << " while we are prepared for removing the range "
                          << m_firstRowToRemove << "..." << m_lastRowToRemove;
            m_updateBatches.clear();
            m_firstRowToRemove = m_lastRowToRemove = -1;
            return;
        }
        else
        {
            NG_MSG_CRITICAL(this) << "GroupingAlarmsProxyModel::srcRowsRemoved(): rows range removed "
                          << first << "..." << last << " while we are not prepared for removing";
            m_updateBatches.clear();
            m_firstRowToRemove = m_lastRowToRemove = -1;
            return;
        }
    }
    QVariant dummyValue;
    // The order of removing several batches from the same group is important, convert map to list and sort
    QList<GroupModelUpdateBatch> allBatches = m_updateBatches.values();
    qSort(allBatches);
    for(auto const &batch: allBatches)
    {
#ifdef DEEP_GROUPING_MODEL_DEBUG
        NG_MSG_WARNING(this) << "beginRemoveRows(), group " << batch.m_group->toString()
                     << ", rows " << batch.m_firstGroupRow << "..." << batch.m_lastGroupRow;
#endif  // DEEP_GROUPING_MODEL_DEBUG
        beginRemoveRows(indexOfGroup(batch.m_group), batch.m_firstGroupRow, batch.m_lastGroupRow);
        const GroupModelUpdateBatch::SourceRowsSet &set = batch.m_sourceSets.value(dummyValue);
        batch.m_group->removeChildrenRows(batch.m_firstGroupRow, batch.m_lastGroupRow);
        for(auto const &alarm: set.m_alarms)
        {
            m_alarmGroups.remove(alarm);
        }
#ifdef DEEP_GROUPING_MODEL_DEBUG
        NG_MSG_WARNING(this) << "endRemoveRows(), group " << batch.m_group->toString();
#endif  // DEEP_GROUPING_MODEL_DEBUG
        endRemoveRows();
        notifyGroupChange(batch.m_group);
    }
    for(int row = last ; row >= first ; row--)
    {
        m_rowAlarms.removeAt(row);
    }
    m_updateBatches.clear();
    m_firstRowToRemove = m_lastRowToRemove = -1;
}

AlarmsProxyModelGroup *GroupingAlarmsProxyModel::placeAlarmToGroup(const QSharedPointer<AlarmInstance> &alarm)
{
    int level = 0;
    AlarmsProxyModelGroup *group = m_rootGroup->findGroupForAlarmAddition(alarm, m_config, level);
    QModelIndex index = indexOfGroup(group);
    int rowsInGroup = group->getChildrenCount();
#ifdef DEEP_GROUPING_MODEL_DEBUG
    NG_MSG_WARNING(this) << "placeAlarmToGroup(" << alarm.toString() << "): group " << group->toString()
                 << ", insert row " << rowsInGroup << " to index " << indexToString(index);
#endif  // DEEP_GROUPING_MODEL_DEBUG
    beginInsertRows(index, rowsInGroup, rowsInGroup);
    AlarmsProxyModelGroup *groupOfAlarm = group->addAlarmToGroup(alarm, m_config, level);
    m_alarmGroups.insert(alarm, groupOfAlarm);
#ifdef DEEP_GROUPING_MODEL_DEBUG
    NG_MSG_WARNING(this) << "placeAlarmToGroup(" << alarm.toString() << "): calling endInsertRows";
#endif  // DEEP_GROUPING_MODEL_DEBUG
    endInsertRows();
    notifyGroupChange(groupOfAlarm);
    return groupOfAlarm;
}

void GroupingAlarmsProxyModel::notifyGroupChange(AlarmsProxyModelGroup *group)
{
    if((group == Q_NULLPTR) || (group == m_rootGroup))
    {
        return;
    }
    QModelIndex index = indexOfGroup(group);
    emit dataChanged(index, index);
    notifyGroupChange(group->getParent());
}