#!/bin/bash

################################################################################
#  This script install the core binaries for AES Development into the PSEN 
#  installation folder in /opt/
#
#  I is supposed to be as sudo
#
################################################################################

# Check if user is root
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

echo "Copying core binaries"

# Copy plugin binaries
cp /home/scada/psen-aes-ng/build-Source-WCCOA_Dev-Debug/libs/AlarmSourcePSEN.so /opt/unicryo/PVSS_projects/psen/installed_components/bin/plugins/AlarmSourcePSEN.so
cp /home/scada/psen-aes-ng/build-Source-WCCOA_Dev-Debug/libs/FilterWidgetPSEN.so /opt/unicryo/PVSS_projects/psen/installed_components/bin/plugins/FilterWidgetPSEN.so

# Copy linux ewo binaries 
cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AS.ewo /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/linux-rhel-x86_64/AS.ewo
cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AsFilterStatistics.ewo /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/linux-rhel-x86_64/AsFilterStatistics.ewo

# Copy windows ewo binaries 
cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AS.ewo /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/windows-64/AS.ewo
cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AsFilterStatistics.ewo /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/windows-64/AsFilterStatistics.ewo

# Copy aes ng core binaries
cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/libAlarmScreenEwoCommon.so /opt/unicryo/PVSS_projects/psen/installed_components/bin/AlarmScreenNg/libAlarmScreenEwoCommon.so
cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/libAlarmModelsNg.so /opt/unicryo/PVSS_projects/psen/installed_components/bin/AlarmScreenNg/libAlarmModelsNg.so

if [[ $1 == "--debug" ]]; then
	echo "Copying debug binaries"

	# Copy plugin debug binaries
	cp /home/scada/psen-aes-ng/build-Source-WCCOA_Dev-Debug/libs/AlarmSourcePSEN.so.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/plugins/AlarmSourcePSEN.so.debug
	cp /home/scada/psen-aes-ng/build-Source-WCCOA_Dev-Debug/libs/FilterWidgetPSEN.so.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/plugins/FilterWidgetPSEN.so.debug

	# Copy linux ewo debug binaries 
	cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AS.ewo.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/linux-rhel-x86_64/AS.ewo.debug
	cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AsFilterStatistics.ewo.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/linux-rhel-x86_64/AsFilterStatistics.ewo.debug

	# Copy windows ewo debug binaries 
	cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AS.ewo.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/windows-64/AS.ewo.debug
	cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/AsFilterStatistics.ewo.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/widgets/windows-64/AsFilterStatistics.ewo.debug

	# Copy aes ng debug binaries
	cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/libAlarmScreenEwoCommon.so.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/AlarmScreenNg/libAlarmScreenEwoCommon.so.debug
	cp /home/scada/aes-ng/build-Source-WCCOA_Dev-Debug/libs/libAlarmModelsNg.so.debug /opt/unicryo/PVSS_projects/psen/installed_components/bin/AlarmScreenNg/libAlarmModelsNg.so.debug
fi
